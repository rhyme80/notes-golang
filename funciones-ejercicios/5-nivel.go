package main

import (
    "fmt"
    "math"
)

type circulo struct {
    radio float64
}

type cuadrado struct {
    longitud float64
}

func (c circulo) area() float64 {
    return math.Pi * c.radio * c.radio
}

func (c circulo) perimetro() float64 {
    return math.Pi * 2 * c.radio
} 

func (c cuadrado) area() float64 {
    return c.longitud * c.longitud
}

func (c cuadrado) perimetro() float64 {
    return 4 * c.longitud
}

type forma interface {
    area() float64
    perimetro() float64
}

func info(f forma) {
    fmt.Println(f.area())
}

func info1(f forma) {
    fmt.Println(f.perimetro())
}

func main() {
    circ := circulo {
        radio: 12.345,
    }

    cua := cuadrado {
        longitud: 5,
    }

    info(circ)
    info(cua)
    info1(cua)
}
