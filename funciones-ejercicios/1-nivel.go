package main

import (
    "fmt"
)

func main() {
    n := foo()
    x, s := bar()
    fmt.Println(n, x, s)

    xi := []int{2, 3, 4, 5, 6, 7, 8, 9}
    y  := sum(xi...)
    fmt.Println("el total almacenado es", y)
}

func foo() int {
    return 42
}

func bar() (int, string) {
    return 1492, "Descubrimiento de America"
}

func sum(x ...int) int {
    fmt.Println(x)
    fmt.Printf("%T\n", x)

    sum := 0
    for i, v := range x {
        sum += v
        fmt.Println("el Valor en el indice", i, "suma", v, "al total, quedando", sum)
    }
    return sum
}
