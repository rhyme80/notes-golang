package main

import (
    "fmt"
)

func main() {
    defer foo()
    fmt.Println("hola")
}

func foo() {
    defer func() {
        fmt.Println("foo diferida corrio")
    }()

    fmt.Println("foo Corrio")
}

