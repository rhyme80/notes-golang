package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Nro de CPU", runtime.NumCPU())
	fmt.Println("Nro de Gorutinas", runtime.NumGoroutine())
	contador := 0

	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			v := contador
			v++
			runtime.Gosched()
			contador = v
			wg.Done()
		}()
		fmt.Println("Nro de Gorutinas", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("cuenta:", contador)
}
