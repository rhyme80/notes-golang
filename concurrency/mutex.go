package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Nro de CPU", runtime.NumCPU())
	fmt.Println("Nro de Gorutinas", runtime.NumGoroutine())
	contador := 0

	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	var mutex sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			mutex.Lock()
			v := contador
			v++
			runtime.Gosched()
			contador = v
			mutex.Unlock()
			wg.Done()
		}()
		fmt.Println("Nro de Gorutinas", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("cuenta:", contador)
}
